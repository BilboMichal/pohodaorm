<?php
declare(strict_types=1);

namespace App\ApiGenerator;

use DI;
use DI\ContainerBuilder as DIContainerBuilder;
use GoetasWebservices\XML\XSDReader\SchemaReader;
use GoetasWebservices\Xsd\XsdToPhp\Jms\PathGenerator\Psr4PathGenerator as JmsPsr4PathGenerator;
use GoetasWebservices\Xsd\XsdToPhp\Jms\YamlConverter;
use GoetasWebservices\Xsd\XsdToPhp\Naming\NamingStrategy;
use GoetasWebservices\Xsd\XsdToPhp\Naming\ShortNamingStrategy;
use GoetasWebservices\Xsd\XsdToPhp\Php\PathGenerator\Psr4PathGenerator as PhpPsr4PathGenerator;
use GoetasWebservices\Xsd\XsdToPhp\Php\PhpConverter;
use GoetasWebservices\Xsd\XsdToPhp\Writer\JMSWriter;
use GoetasWebservices\Xsd\XsdToPhp\Writer\PHPClassWriter;
use GoetasWebservices\Xsd\XsdToPhp\Writer\PHPWriter;
use Laminas\Code\Generator\ClassGenerator;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * DI container builder for generator app
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class ContainerBuilder
{

    public static function build(array $config): ContainerInterface
    {
        $builder = new DIContainerBuilder();
        $container = $builder->build();

        // ------------------------------------------------------------
        // services
        // 
        // PSR-3 logger
        $container->set(LoggerInterface::class, function (): LoggerInterface {
            return NullLogger;
        });

        // Naming strategy
        $container->set(NamingStrategy::class, DI\autowire(ShortNamingStrategy::class));

        // XSD reader
        $container->set(SchemaReader::class, DI\autowire(SchemaReader::class));

        // PHP converter
        $container->set(PhpConverter::class,
            function (Config $cf, NamingStrategy $nstrategy, LoggerInterface $logger = null): PhpConverter {
                $phpConverter = new PhpConverter($nstrategy, $logger);
                foreach ($cf->getNamespacesMap() as $xsdNs => $phpNs) {
                    $phpConverter->addNamespace($xsdNs, $phpNs);
                }
                return $phpConverter;
            });

        // YML converter
        $container->set(YamlConverter::class,
            function (Config $cf, NamingStrategy $nstrategy): YamlConverter {
                $yamlConverter = new YamlConverter($nstrategy);
                foreach ($cf->getNamespacesMap() as $xsdNs => $phpNs) {
                    $yamlConverter->addNamespace($xsdNs, $phpNs);
                }
                return $yamlConverter;
            });

        // PHP writer
        $container->set(PHPWriter::class,
            function (Config $cf, LoggerInterface $logger = null): PHPWriter {
                return new PHPWriter(new PHPClassWriter(new PhpPsr4PathGenerator($cf->getPhpClassesMap()), $logger),
                new ClassGenerator, $logger);
            });

        // JMS writer
        $container->set(JMSWriter::class,
            function (Config $cf, LoggerInterface $logger = null): JMSWriter {
                return new JMSWriter(new JmsPsr4PathGenerator($jmsClassesMap), $logger);
            });
    }
}