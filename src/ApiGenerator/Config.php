<?php
declare(strict_types=1);

namespace App\ApiGenerator;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Config
{
    /** @var string */
    private $xsdBaseUrl;

    /** @var string */
    private $xsdDir;

    /** @var string */
    private $namespaceApiBase;

    /** @var array */
    private $namespacesMap;

    /** @var array */
    private $phpClassesMap;

    /** @var array */
    private $jmsClassesMap;

    public function __construct(string $xsdBaseUrl, string $xsdDir, string $namespaceApiBase)
    {
        $this->xsdBaseUrl = rtrim($xsdBaseUrl, '/');
        $this->xsdDir = $xsdDir;
        $this->namespaceApiBase = $namespaceApiBase;
    }

    /**
     *
     * @return array
     */
    public function getXsdFilenames(): array
    {
        return array_map(function ($xsdFilename): string {
            return pathinfo($xsdFilename, PATHINFO_FILENAME);
        },
            array_filter(scandir($this->xsdDir),
                function (string $file): bool {
                    return pathinfo($file, PATHINFO_EXTENSION) === 'xsd';
                }));
    }

    private function calculateMaps()
    {
        $this->namespacesMap = [];
        $this->phpClassesMap = [];
        $this->jmsClassesMap = [];
        foreach ($this->getXsdFilenames() as $xsdFilename) {
            $name = str_replace('_', '', ucwords($xsdFilename, '_'));
            $phpNamespace = $this->namespaceApiBase.'\\'.$name;
            $this->namespacesMap[$this->xsdBaseUrl.'/'.$xsdFilename] = $phpNamespace;
            $this->phpClassesMap[$phpNamespace] = $phpDirBase.'/'.$name;
            $this->jmsClassesMap[$phpNamespace] = $jmsDirBase.'/'.$name;
        }
    }

    /**
     * XSD namespace => PHP class namespace
     *
     * @return array
     */
    public function getNamespacesMap(): array
    {
        if (!isset($this->namespacesMap)) {
            $this->calculateMaps();
        }

        return $this->namespacesMap;
    }

    /**
     * PHP class namespace => PHP class file
     *
     * @return array
     */
    public function getPhpClassesMap(): array
    {
        if (!isset($this->phpClassesMap)) {
            $this->calculateMaps();
        }

        return $this->phpClassesMap;
    }

    /**
     * PHP class namespace => JMS file
     *
     * @return array
     */
    public function getJmsClassesMap(): array
    {
        if (!isset($this->jmsClassesMap)) {
            $this->calculateMaps();
        }

        return $this->jmsClassesMap;
    }
}