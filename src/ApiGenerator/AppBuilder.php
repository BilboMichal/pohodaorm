<?php
declare(strict_types=1);

namespace App\ApiGenerator;

use Symfony\Component\Console\Application;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class AppBuilder
{

    public static function build(): Application
    {
        $app = new Application('API classes generator');
        return $app;
    }
}