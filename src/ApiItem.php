<?php
declare(strict_types=1);

namespace Bilbofox\PohodaOrm;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Serializable;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
abstract class ApiItem implements Serializable
{
    /** @var SerializerInterface @internal */
    private $_serializer;

    protected function createDefaultSerializer(): SerializerInterface
    {
        $serializer = SerializerBuilder::create()
//            ->addMetadataDir()
            ->build();

        return $serializer;
    }

    public function getSerializer(): SerializerInterface
    {
        if (!isset($this->_serializer)) {
            $this->_serializer = $this->createDefaultSerializer();
        }

        return $this->_serializer;
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        $this->_serializer = $serializer;
        return $this;
    }

    public function serialize(): string
    {
        $this->getSerializer()->serialize($this, 'xml');
    }

    public function unserialize(string $serialized)
    {
        return $this->getSerializer()->deserialize($serialized, 'xml');
    }

    public function __toString(): string
    {
        return $this->serialize();
    }
}