<?php

use GoetasWebservices\XML\XSDReader\Schema\Schema;
use GoetasWebservices\XML\XSDReader\SchemaReader;
use GoetasWebservices\Xsd\XsdToPhp\Jms\PathGenerator\Psr4PathGenerator as JmsPsr4PathGenerator;
use GoetasWebservices\Xsd\XsdToPhp\Jms\YamlConverter;
use GoetasWebservices\Xsd\XsdToPhp\Jms\YamlValidatorConverter;
use GoetasWebservices\Xsd\XsdToPhp\Naming\ShortNamingStrategy;
use GoetasWebservices\Xsd\XsdToPhp\Php\ClassGenerator;
use GoetasWebservices\Xsd\XsdToPhp\Php\PathGenerator\Psr4PathGenerator as PhpPsr4PathGenerator;
use GoetasWebservices\Xsd\XsdToPhp\Php\PhpConverter;
use GoetasWebservices\Xsd\XsdToPhp\Php\Structure\PHPClass;
use GoetasWebservices\Xsd\XsdToPhp\Writer\JMSWriter;
use GoetasWebservices\Xsd\XsdToPhp\Writer\PHPClassWriter;
use GoetasWebservices\Xsd\XsdToPhp\Writer\PHPWriter;
use Nette\Utils\FileSystem;
use Psr\Log\NullLogger;

require_once __DIR__.'/../vendor/autoload.php';

// config
// -----------------------------------------------------

$xsdDir = __DIR__.'/../xsd';
$xsdFiles = array_filter(scandir($xsdDir), fn($file) => pathinfo($file, PATHINFO_EXTENSION) === 'xsd');
$xsdBaseUrl = 'http://www.stormware.cz/schema/version_2/';

$namespaceBase = 'Bilbofox\\PohodaOrm';
$namespaceApiBase = $namespaceBase . '\\Api';
$phpDirBase = __DIR__.'/../src/Api';
$jmsDirBase = __DIR__.'/../src/Jms';

// logger
$logger = new NullLogger;

// XSD namespace => PHP class namespace
$namespacesMap = [];
// PHP class namespace => PHP class file
$phpClassesMap = [];
// PHP class namespace => JMS file
$jmsClassesMap = [];
foreach ($xsdFiles as $xsdFile) {
    $name = str_replace('_', '', ucwords(pathinfo($xsdFile, PATHINFO_FILENAME), '_'));
    $phpNamespace = $namespaceApiBase.'\\'.$name;
    $namespacesMap[$xsdBaseUrl.$xsdFile] = $phpNamespace;
    $phpClassesMap[$phpNamespace] = $phpDirBase.'/'.$name;
    $jmsClassesMap[$phpNamespace] = $jmsDirBase.'/'.$name;
}

$namingStrategy = new ShortNamingStrategy();

// xsd reader
$xsdReader = new SchemaReader;

// php converter
$phpConverter = new PhpConverter($namingStrategy);

// yml converter
$yamlConverter = new YamlConverter($namingStrategy);

// yml validator converter
$yamlValidatorConverter = new YamlValidatorConverter($namingStrategy);

foreach ($namespacesMap as $xsdNamespace => $phpNamespace) {
    $phpConverter->addNamespace($xsdNamespace, $phpNamespace);
    $yamlConverter->addNamespace($xsdNamespace, $phpNamespace);
}

// php writer
$phpWriter = new PHPWriter(new PHPClassWriter(new PhpPsr4PathGenerator($phpClassesMap), $logger), new ClassGenerator,
    $logger);

// jms writer
$jmlWriter = new JMSWriter(new JmsPsr4PathGenerator($jmsClassesMap), $logger);

$xsdSchemas = array_map(fn(string $xsdFile): Schema => $xsdReader->readFile($xsdDir.'/'.$xsdFile), $xsdFiles);

// run
// -----------------------------------------------------
// 
// Delete old files
FileSystem::delete($phpDirBase);
FileSystem::createDir($phpDirBase);
FileSystem::delete($jmsDirBase);
FileSystem::createDir($jmsDirBase);

// TODO delete
$phpClasses = $phpConverter->convert($xsdSchemas);
foreach ($phpClasses as $phpClass) {
    if ($phpClass->getExtends() === null) {
        $phpClass->setExtends(new PHPClass('ApiItem', $namespaceBase));
    }
}

$jmsConfigs = $yamlConverter->convert($xsdSchemas);

$phpWriter->write($phpClasses);
$jmlWriter->write($jmsConfigs);

//$namingStrategy = new ShortNamingStrategy();
//$phpConverter = new PhpConverter($namingStrategy);

//$return = $phpConverter->convert();


$stock = new \Bilbofox\PohodaOrm\Api\Stock\Stock;

$stockHeader = new \Bilbofox\PohodaOrm\Api\Stock\StockHeaderType;
$stockHeader->setCode('foo');

$stock->setStockHeader($stockHeader);

echo (string) $stock;